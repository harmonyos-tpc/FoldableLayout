package com.alexvasilkov.foldablelayout.entry;

import com.alexvasilkov.foldablelayout.FoldableItem;
import com.alexvasilkov.foldablelayout.UnfoldableView;
import com.alexvasilkov.foldablelayout.entry.items.Items;
import com.alexvasilkov.foldablelayout.entry.util.ResUtil;
import com.alexvasilkov.foldablelayout.shading.GlanceFoldShading;
import com.alexvasilkov.foldablelayout.shading.SimpleFoldShading;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;
import ohos.agp.utils.Color;
import ohos.media.image.PixelMap;

import java.util.List;

public class UnfoldableDetailsAbility extends Ability {
    private ListContainer listContainer;
    private ListProvider listProvider;
    private UnfoldableView unfoldableView;
    private List<FoldableItem> foldableItems;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_unfoldabledetails);
        PixelMap glance = ResUtil.getPixelMap(getContext(), ResourceTable.Media_unfold_glance).get();
        foldableItems = Items.getFoldableItems(getContext());
        unfoldableView = (UnfoldableView) findComponentById(ResourceTable.Id_unfoldableview);
        unfoldableView.setDetailShading(new SimpleFoldShading());
        unfoldableView.setCoverShading(new GlanceFoldShading(glance));
        listContainer = (ListContainer) findComponentById(ResourceTable.Id_list);
        listContainer.setBoundaryColor(Color.TRANSPARENT);
        listContainer.setBoundarySwitch(true);
        listContainer.setBoundaryThickness(Items.dp2px(12, getContext()));
        listProvider = new ListProvider(foldableItems, getContext(), new ListProvider.FoldableItemOpenListener() {
            @Override
            public void open(Component component, int index) {
                unfoldableView.setVisibility(Component.VISIBLE);
                unfoldableView.unfold(foldableItems.get(index), Items.getUnfoldableDetail(getContext(), index), component);
                listContainer.setEnabled(false);
                listContainer.setTouchFocusable(false);
            }
        });
        unfoldableView.setOnFoldingListener(new UnfoldableView.SimpleFoldingListener() {
            @Override
            public void onFoldedBack(UnfoldableView unfoldableView) {
                super.onFoldedBack(unfoldableView);
                listContainer.setEnabled(true);
                listContainer.setTouchFocusable(true);
            }
        });
        listContainer.setItemProvider(listProvider);
    }
}
