package com.alexvasilkov.foldablelayout.entry;

import com.alexvasilkov.foldablelayout.FoldableListLayout;
import com.alexvasilkov.foldablelayout.entry.items.Items;
import com.alexvasilkov.foldablelayout.entry.util.ResUtil;
import com.alexvasilkov.foldablelayout.shading.GlanceFoldShading;
import com.alexvasilkov.foldablelayout.shading.SimpleFoldShading;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.media.image.PixelMap;

public class FoldableListAbility extends Ability {
    private FoldableListLayout foldableListLayout;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_foldablelist);
        foldableListLayout = (FoldableListLayout) findComponentById(ResourceTable.Id_foldablelist);
        foldableListLayout.setFoldShading(new SimpleFoldShading());
        foldableListLayout.setAdapter(new FoldableListLayout.FoldableAdapter(Items.getFoldableItems(getContext())));
    }

}
