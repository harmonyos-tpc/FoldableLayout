package com.alexvasilkov.foldablelayout.entry;

import com.alexvasilkov.foldablelayout.FoldableItem;
import com.alexvasilkov.foldablelayout.entry.items.Items;
import ohos.agp.components.*;
import ohos.agp.render.Canvas;
import ohos.app.Context;

import java.util.List;

public class ListProvider extends BaseItemProvider {
    private List<FoldableItem> foldableItems;
    private Context context;
    private FoldableItemOpenListener foldableItemOpenListener;

    public ListProvider(List<FoldableItem> foldableItems, Context context, FoldableItemOpenListener foldableItemOpenListener) {
        this.foldableItemOpenListener = foldableItemOpenListener;
        this.foldableItems = foldableItems;
        this.context = context;
    }

    @Override
    public int getCount() {
        return foldableItems.size();
    }

    @Override
    public Object getItem(int i) {
        return foldableItems.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        if (component == null || !component.getTag().equals(i)) {
            component = new Component(context);
            component.setLayoutConfig(new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, Items.dp2px(200, context)));
            component.addDrawTask((component1, canvas) -> {
                foldableItems.get(i).onDraw(canvas, component1.getWidth(), component1.getHeight());
            });
            component.setTag(i);
            component.setClickedListener(component1 -> {
                foldableItemOpenListener.open(component1, i);
            });
        } else {
            component.invalidate();
        }
        return component;
    }

    public interface FoldableItemOpenListener {
        public void open(Component component, int index);
    }
}
