package com.alexvasilkov.foldablelayout.entry;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Text;

public class MainAbility extends Ability {
    private Text unfoldableDetails;
    private Text foldableList;
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        unfoldableDetails = (Text)findComponentById(ResourceTable.Id_unfoldable);
        foldableList = (Text)findComponentById(ResourceTable.Id_foldable);
        unfoldableDetails.setClickedListener(component -> {
            Intent intent2 = new Intent();
            Operation operation = new Intent.OperationBuilder().withAction("action.unfoldableDetails").build();
            intent2.setOperation(operation);
            startAbility(intent2);
        });
        foldableList.setClickedListener(component -> {
            Intent intent2 = new Intent();
            Operation operation = new Intent.OperationBuilder().withAction("action.folablelist").build();
            intent2.setOperation(operation);
            startAbility(intent2);
        });
    }

}
