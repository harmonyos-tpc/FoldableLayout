package com.alexvasilkov.foldablelayout.entry.items;

import com.alexvasilkov.foldablelayout.FoldableItem;
import com.alexvasilkov.foldablelayout.entry.ResourceTable;
import com.alexvasilkov.foldablelayout.entry.util.ResUtil;
import ohos.agp.render.*;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.media.image.PixelMap;

import java.util.ArrayList;
import java.util.List;

public class Items {
    public static String[] titles = new String[]{
            "Starry Night",
            "Cafe Terrace at Night",
            "Starry Night Over the Rhone",
            "Sunflowers",
            "Almond Blossoms"
    };

    public static String[] years = new String[]{
            "1889",
            "1888",
            "1888",
            "1888",
            "1890",
    };

    public static String[] locations = new String[]{
            "Museum of Modern Art, New York City",
            "Kröller-Müller Museum, Otterlo",
            "Musée d'Orsay, Paris",
            "National Gallery, London",
            "Van Gogh Museum, Amsterdam",
    };

    public static int[] images = new int[]{
            ResourceTable.Media_starry_night,
            ResourceTable.Media_cafe_terrace,
            ResourceTable.Media_starry_night_over_the_rhone,
            ResourceTable.Media_sunflowers,
            ResourceTable.Media_almond_blossoms,
    };

    public static FoldableItem getUnfoldableDetail(Context context, int index) {
        if (index < 0 || index >= images.length) {
            return null;
        }
        PixelMap pixelMap = ResUtil.getPixelMap(context, images[index]).get();
        Paint paint = new Paint();
        FoldableItem foldableItem = new FoldableItem() {
            @Override
            public void onDraw(Canvas canvas, int width, int height) {
                paint.setColor(new Color(0xff3f3f3f));
                canvas.drawRect(new RectFloat(0, 0, width, height), paint);
                canvas.drawPixelMapHolderRect(new PixelMapHolder(pixelMap),
                        getDrawRectFloat(pixelMap, width, height / 2),
                        new RectFloat(0, 0, width, height / 2), paint);
                paint.setColor(Color.WHITE);
                canvas.drawLine(0, height / 2, width, height / 2, paint);
                paint.setTextSize(dp2px(18, context));
                canvas.drawText(paint, titles[index], dp2px(16, context), height / 2 + dp2px(12, context) + 0.75f * paint.getTextSize());
                canvas.drawLine(dp2px(16, context), height / 2 + dp2px(36, context), width - dp2px(16, context), height / 2 + dp2px(36, context), paint);
                paint.setTextSize(dp2px(14, context));
                paint.setFont(Font.DEFAULT_BOLD);
                canvas.drawText(paint, "Year:", dp2px(16, context), height / 2 + dp2px(48, context) + 0.75f * paint.getTextSize());
                float stringWidth = paint.measureText("Year:");
                paint.setFont(Font.DEFAULT);
                canvas.drawText(paint, years[index], dp2px(16, context) + stringWidth, height / 2 + dp2px(48, context) + 0.75f * paint.getTextSize());
                paint.setFont(Font.DEFAULT_BOLD);
                canvas.drawText(paint, "Locations:", dp2px(16, context), height / 2 + dp2px(64, context) + 0.75f * paint.getTextSize());
                stringWidth = paint.measureText("Locations:");
                paint.setFont(Font.DEFAULT);
                canvas.drawText(paint, locations[index], dp2px(16, context) + stringWidth, height / 2 + dp2px(64, context) + 0.75f * paint.getTextSize());
            }
        };
        return foldableItem;
    }

    public static List<FoldableItem> getFoldableItems(Context context) {
        List<PixelMap> pixelMaps = new ArrayList<>();
        List<FoldableItem> foldableItems = new ArrayList<>();
        Paint paint = new Paint();
        Paint elevationPaint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setTextSize(dp2px(15, context));
        for (int i = 0; i < images.length && i < titles.length; i++) {
            PixelMap pixelMap = ResUtil.getPixelMap(context, images[i]).get();
            pixelMaps.add(pixelMap);
        }
        for (int i = 0; i < images.length && i < titles.length; i++) {

            int index = i;
            FoldableItem foldableItem = new FoldableItem() {
                @Override
                public void onDraw(Canvas canvas, int width, int height) {
                    canvas.drawRect(new RectFloat(0, 0, width, height), paint);
                    PixelMap pixelMap = pixelMaps.get(index);
                    int padding = dp2px(4, context);
                    int screenWidth = width - padding * 2;
                    int screenHeight = height - padding * 2;
                    canvas.drawPixelMapHolderRect(new PixelMapHolder(pixelMap),
                            getDrawRectFloat(pixelMap, screenWidth, screenHeight),
                            new RectFloat(padding, padding, width - padding, height - padding), paint);

                    int shadowHeight = dp2px(40, context);
                    LinearShader gradient =
                            new LinearShader(
                                    new Point[]{new Point(0, height - padding - shadowHeight), new Point(0, height - padding)},
                                    new float[]{0f, 0.3f, 1f},
                                    new Color[]{new Color(0x00000000), new Color(0x33000000), new Color(0x88000000)},
                                    Shader.TileMode.CLAMP_TILEMODE);

                    elevationPaint.setShader(gradient, Paint.ShaderType.LINEAR_SHADER);
                    canvas.drawRect(new RectFloat(padding, screenHeight - padding - shadowHeight, width - padding, height - padding), elevationPaint);
                    canvas.drawText(paint, titles[index], padding + dp2px(8, context), screenHeight - padding - dp2px(8, context));
                }
            };
            foldableItems.add(foldableItem);
        }
        return foldableItems;
    }

    public static int dp2px(float dp, Context context) {
        return (int) (context.getResourceManager().getDeviceCapability().screenDensity / 160 * dp + 0.5f);
    }

    public static RectFloat getDrawRectFloat(PixelMap pixelMap, int width, int height) {
        int pixelMapWidth = pixelMap.getImageInfo().size.width;
        int pixelMapHeight = pixelMap.getImageInfo().size.height;
        return getDrawRectFloat(pixelMapWidth, pixelMapHeight, width, height);
    }

    public static RectFloat getDrawRectFloat(int pixelMapWidth, int pixelMapHeight, int width, int height) {
        if (pixelMapWidth * height > pixelMapHeight * width) {
            return new RectFloat((pixelMapWidth - width * pixelMapHeight / height) / 2, 0,
                    (pixelMapWidth + width * pixelMapHeight / height) / 2, pixelMapHeight);
        } else {
            return new RectFloat(0, (pixelMapHeight - height * pixelMapWidth / width) / 2,
                    pixelMapWidth, (pixelMapHeight + height * pixelMapWidth / width) / 2);
        }
    }
}
