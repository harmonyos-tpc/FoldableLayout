package com.alexvasilkov.foldablelayout;

import com.alexvasilkov.foldablelayout.shading.FoldShading;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.ThreeDimView;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.Matrix;
import ohos.agp.utils.RectFloat;

public abstract class FoldableItem {
    private static final int CAMERA_DISTANCE = 48;
    private Paint paint = new Paint();

    public abstract void onDraw(Canvas canvas, int width, int height);

    public void draw(Canvas canvas, int width, int height, float rotation, int gravity, FoldShading foldShading) {
        int saveCount = canvas.save();
        double sin = Math.abs(Math.sin(Math.toRadians(rotation)));
        float dw = (float) (height * sin);
        float scaleFactor = width / (width + dw);
        if (gravity == LayoutAlignment.TOP) {
            if (rotation != 0) {
                ThreeDimView threeDimView = new ThreeDimView();
                Matrix matrix = new Matrix();
                threeDimView.rotateX(rotation);
                threeDimView.getMatrix(matrix);
                matrix.preTranslate(-width / 2, -height / 2);
                matrix.postTranslate(width / 2, height / 2);
                canvas.concat(matrix);
            }
            RectFloat topPart = new RectFloat(0, 0, width, height / 2);
            if (rotation == 0) {
                canvas.saveLayer(topPart, paint);
            } else {
                canvas.scale(1, scaleFactor, width / 2, height / 2);
            }
            if (foldShading != null) {
                foldShading.onPreDraw(canvas, topPart, rotation, gravity);
            }
            onDraw(canvas, width, height);
            if (foldShading != null) {
                foldShading.onPostDraw(canvas, topPart, rotation, gravity);
            }
        } else if (gravity == LayoutAlignment.BOTTOM) {
            if (rotation != 0) {
                ThreeDimView threeDimView = new ThreeDimView();
                Matrix matrix = new Matrix();
                threeDimView.rotateX(rotation);
                threeDimView.getMatrix(matrix);
                matrix.preTranslate(-width / 2, -height / 2);
                matrix.postTranslate(width / 2, height / 2);
                canvas.concat(matrix);
            }
            RectFloat bottomPart = new RectFloat(0, height / 2, width, height);
            if (rotation == 0) {
                canvas.saveLayer(bottomPart, paint);
            } else {
                canvas.scale(1, scaleFactor, width / 2, height / 2);
            }
            if (foldShading != null) {
                foldShading.onPreDraw(canvas, bottomPart, rotation, gravity);
            }
            onDraw(canvas, width, height);
            if (foldShading != null) {
                foldShading.onPostDraw(canvas, bottomPart, rotation, gravity);
            }
        } else if (gravity == LayoutAlignment.START) {
            if (rotation != 0) {
                ThreeDimView threeDimView = new ThreeDimView();
                Matrix matrix = new Matrix();
                threeDimView.rotateX(rotation);
                threeDimView.getMatrix(matrix);
                matrix.preTranslate(-width / 2, 0);
                matrix.postTranslate(width / 2, 0);
                canvas.concat(matrix);
            }
            RectFloat rectFloat = new RectFloat(0, 0, width, height);
            if (rotation != 0) {
                canvas.scale(1, scaleFactor, width / 2, 0);
            }
            if (foldShading != null) {
                foldShading.onPreDraw(canvas, rectFloat, rotation, gravity);
            }
            onDraw(canvas, width, height);
            if (foldShading != null) {
                foldShading.onPostDraw(canvas, rectFloat, rotation, gravity);
            }
        }
        canvas.restoreToCount(saveCount);
    }
}
