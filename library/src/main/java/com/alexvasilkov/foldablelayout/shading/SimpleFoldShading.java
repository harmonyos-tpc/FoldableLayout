package com.alexvasilkov.foldablelayout.shading;

import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.RectFloat;

public class SimpleFoldShading implements FoldShading {

    private static final int SHADOW_COLOR = Color.BLACK.getValue();
    private static final int SHADOW_MAX_ALPHA = 192;

    private final Paint solidShadow;

    public SimpleFoldShading() {
        solidShadow = new Paint();
        solidShadow.setColor(new Color(SHADOW_COLOR));
    }

    @Override
    public void onPreDraw(Canvas canvas, RectFloat bounds, float rotation, int gravity) {
        // No-op
    }

    @Override
    public void onPostDraw(Canvas canvas, RectFloat bounds, float rotation, int gravity) {
        float intensity = getShadowIntensity(rotation, gravity);
        if (intensity > 0f) {
            float alpha = SHADOW_MAX_ALPHA * intensity / 255f;
            solidShadow.setAlpha(alpha);
            canvas.drawRect(bounds, solidShadow);
        }
    }

    private float getShadowIntensity(float rotation, int gravity) {
        float intensity = 0f;
        if (gravity == LayoutAlignment.TOP) {
            if (rotation > -90f && rotation < 0f) { // (-90; 0) - Rotation is applied
                intensity = -rotation / 90f;
            }
        } else {
            if (rotation > 0f && rotation < 90f) { // (0; 90) - Rotation is applied
                intensity = rotation / 90f;
            }
        }
        return intensity;
    }

}
