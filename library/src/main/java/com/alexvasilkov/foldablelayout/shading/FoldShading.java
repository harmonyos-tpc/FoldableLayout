package com.alexvasilkov.foldablelayout.shading;

import ohos.agp.render.Canvas;
import ohos.agp.utils.RectFloat;

public interface FoldShading {
    void onPreDraw(Canvas canvas, RectFloat bounds, float rotation, int gravity);

    void onPostDraw(Canvas canvas, RectFloat bounds, float rotation, int gravity);
}
