package com.alexvasilkov.foldablelayout;

import com.alexvasilkov.foldablelayout.shading.FoldShading;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;

import java.util.List;

public class UnfoldableView extends Component implements Component.DrawTask, Component.TouchEventListener {
    private static final int STATE_FOLDED = 0;
    private static final int STATE_UNFOLDING = 1;
    private static final int STATE_UNFOLDED = 2;
    private static final int STATE_FOLDING = 3;
    private int state = STATE_FOLDED;

    private static final long ANIMATION_DURATION_PER_ITEM = 600L;
    private boolean isGesturesEnabled = true;
    private FoldableItem coverFoldableItem;
    private FoldableItem detailFoldableItem;
    private FoldShading coverShading;
    private FoldShading detailShading;
    private Component coverComponent;

    private AnimatorValue animator;

    private float foldRotation;
    private float minRotation = 0;
    private float maxRotation = 180;
    private float touchX;
    private float touchY;

    private long lastTime;
    private int speed;

    private float lastFoldRotation;

    private OnFoldingListener foldingListener;

    private FoldableListLayout.OnFoldRotationListener foldRotationListener;

    public UnfoldableView(Context context) {
        this(context, null, null);
    }

    public UnfoldableView(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    public UnfoldableView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init();
    }

    private void init() {
        animator = new AnimatorValue();
        addDrawTask(this);
        setTouchEventListener(this);
    }

    public void unfold(FoldableItem coverFoldableItem, FoldableItem detailFoldableItem, Component coverComponent) {
        if (state != STATE_FOLDED) return;
        this.coverFoldableItem = coverFoldableItem;
        this.detailFoldableItem = detailFoldableItem;
        this.coverComponent = coverComponent;
        foldRotation = 0;
        invalidate();
        setState(STATE_UNFOLDING);
        coverComponent.setVisibility(Component.INVISIBLE);
        animateFold(180);
    }

    public void setFoldableItem(FoldableItem coverFoldableItem, FoldableItem detailFoldableItem, Component coverComponent) {
        this.coverFoldableItem = coverFoldableItem;
        this.detailFoldableItem = detailFoldableItem;
        this.coverComponent = coverComponent;
        invalidate();
    }

    public void setCoverFoldableItem(FoldableItem coverFoldableItem, Component coverComponent){
        this.coverFoldableItem = coverFoldableItem;
        this.coverComponent = coverComponent;
        invalidate();
    }

    public void setDetailFoldableItem(FoldableItem detailFoldableItem){
        this.detailFoldableItem = detailFoldableItem;
        invalidate();
    }

    public void setOnFoldingListener(OnFoldingListener listener) {
        this.foldingListener = listener;
    }

    public void setOnFoldRotationListener(FoldableListLayout.OnFoldRotationListener listener) {
        foldRotationListener = listener;
    }

    public void setCoverShading(FoldShading shading) {
        coverShading = shading;
        invalidate();
    }

    public void setDetailShading(FoldShading shading) {
        detailShading = shading;
        invalidate();
    }

    public void setGesturesEnabled(boolean isGesturesEnabled) {
        this.isGesturesEnabled = isGesturesEnabled;
    }

    public float getFoldRotation() {
        return foldRotation;
    }

    public final void setFoldRotation(float rotation) {
        setFoldRotation(rotation, false);
    }

    protected void setFoldRotation(float rotation, boolean isFromUser) {
        if (isFromUser) {
            animator.cancel();
        }

        float stage = rotation / 180f;
        rotation = Math.min(Math.max(minRotation, rotation), maxRotation);
        foldRotation = rotation;
        if (foldRotationListener != null) {
            foldRotationListener.onFoldRotation(rotation, isFromUser);
        }

        if (foldingListener != null) {
            foldingListener.onFoldProgress(this, stage);
        }

        final float lastRotation = lastFoldRotation;
        lastFoldRotation = rotation;

        if (rotation > lastRotation) {
            setState(STATE_UNFOLDING);
        }

        if (rotation < lastRotation) {
            setState(STATE_FOLDING);
        }

        if (rotation == 180f) {
            setState(STATE_UNFOLDED);
        }

        if (rotation == 0f && state == STATE_FOLDING) {
            setState(STATE_FOLDED);
        }
        invalidate();
    }

    public int getPosition() {
        return Math.round(foldRotation / 180f);
    }

    protected void animateFold(float to) {
        animateFold(to, 180);
    }

    private void animateFold(float to, int speed) {
        final float from = getFoldRotation();
        final long duration = (long) Math.abs(ANIMATION_DURATION_PER_ITEM * (to - from) / speed);

        animator.stop();

        animator.cancel();
        animator.setValueUpdateListener((animatorValue, v) -> {
            float rotation = from + (to - from) * v;
            setFoldRotation(rotation);
        });
        animator.setDuration(duration);
        animator.start();
    }

    private void setState(int state) {
        if (this.state != state) {
            this.state = state;

            if (state == STATE_FOLDED) {
                coverComponent.setVisibility(Component.VISIBLE);
                setVisibility(Component.HIDE);
            }

            if (foldingListener != null) {
                switch (state) {
                    case STATE_UNFOLDING:
                        foldingListener.onUnfolding(this);
                        break;
                    case STATE_FOLDING:
                        foldingListener.onFoldingBack(this);
                        break;
                    case STATE_UNFOLDED:
                        foldingListener.onUnfolded(this);
                        break;
                    case STATE_FOLDED:
                        foldingListener.onFoldedBack(this);
                        break;
                    default:
                        // Nothing
                }
            }
        }
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        if (coverFoldableItem == null && detailFoldableItem == null && coverComponent == null) {
            return;
        }
        float detailMinScaleX = coverComponent.getWidth() * 1f / getWidth();
        float detailMinScaleY = coverComponent.getHeight() * 2f / getHeight();
        float detailScaleX = detailMinScaleX + (1 - detailMinScaleX) * foldRotation / 180;
        float detailScaleY = 1;
        if (foldRotation < 90) {
            detailScaleY = detailMinScaleY + (1 - detailMinScaleY) * foldRotation / 90;
        }
        float itemMaxScaleX = getWidth() * 1f / coverComponent.getWidth();
        float itemMaxScaleY = getHeight() * 0.5f / coverComponent.getHeight();
        float itemScaleX = 1 + (itemMaxScaleX - 1) * foldRotation / 180;
        float itemScaleY = 1;
        if (foldRotation < 90) {
            itemScaleY = 1 + (itemMaxScaleY - 1) * foldRotation / 90;
        }
        float offsetY = coverComponent.getLocationOnScreen()[1] - getHeight() / 2 - getLocationOnScreen()[1];
        canvas.translate(0, offsetY * (180 - foldRotation) / 180);

        if (about(foldRotation, 90)) {
            canvas.scale(detailScaleX, detailScaleY, getWidth() / 2, getHeight() / 2);
            detailFoldableItem.draw(canvas, getWidth(), getHeight(), 0, LayoutAlignment.BOTTOM, detailShading);
            canvas.drawLine(0, getHeight() / 2, getWidth(), getHeight() / 2, new Paint(), Color.BLACK);
        } else if (foldRotation < 90) {
            int saveCount = canvas.save();
            canvas.scale(detailScaleX, detailScaleY, getWidth() / 2, getHeight() / 2);
            detailFoldableItem.draw(canvas, getWidth(), getHeight(), 0, LayoutAlignment.BOTTOM, detailShading);
            canvas.restoreToCount(saveCount);
            canvas.scale(itemScaleX, itemScaleY, getWidth() / 2, getHeight() / 2);
            canvas.translate(coverComponent.getLocationOnScreen()[0], getHeight() / 2);
            coverFoldableItem.draw(canvas, coverComponent.getWidth(), coverComponent.getHeight(), foldRotation, LayoutAlignment.START, coverShading);
        } else {
            canvas.scale(detailScaleX, detailScaleY, getWidth() / 2, getHeight() / 2);
            detailFoldableItem.draw(canvas, getWidth(), getHeight(), foldRotation - 180, LayoutAlignment.TOP, detailShading);
            detailFoldableItem.draw(canvas, getWidth(), getHeight(), 0, LayoutAlignment.BOTTOM, null);
        }
    }

    private boolean about(float a, float b) {
        if (Math.abs(a - b) < 0.1f) {
            return true;
        }
        return false;
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        if (state == STATE_FOLDED || !isGesturesEnabled) return false;
        if (animator != null) {
            animator.stop();
        }
        if (touchEvent.getPointerCount() == 1) {
            if (touchEvent.getAction() == TouchEvent.PRIMARY_POINT_DOWN) {
                touchX = getTouchX(touchEvent, 0);
                touchY = getTouchY(touchEvent, 0);
                speed = 0;
                lastTime = System.currentTimeMillis();
            } else if (touchEvent.getAction() == TouchEvent.POINT_MOVE) {
                float offset = getTouchY(touchEvent, 0) - touchY;
                float rotation = -180f * offset * 2 / getHeight();
                setFoldRotation(foldRotation + rotation, true);
                if (lastTime != 0) {
                    speed = (int) (rotation * 1000 / (System.currentTimeMillis() - lastTime));
                }
                lastTime = System.currentTimeMillis();
                touchX = getTouchX(touchEvent, 0);
                touchY = getTouchY(touchEvent, 0);
            } else if (touchEvent.getAction() == TouchEvent.PRIMARY_POINT_UP) {
                int from = (int) foldRotation;
                if (speed > 180 || speed < -180) {
                    int to;
                    if (speed > 0) {
                        to = getNextRotation(foldRotation);
                    } else {
                        to = getLastRotation(foldRotation);
                    }
                    animateFold(to, speed);
                } else {
                    int to;
                    if (Math.abs(getNextRotation(foldRotation) - from) < Math.abs(getLastRotation(foldRotation) - from)) {
                        to = getNextRotation(foldRotation);
                    } else {
                        to = getLastRotation(foldRotation);
                    }
                    animateFold(to);
                }
            }
        }
        return true;
    }


    public static class FoldableAdapter {
        private List<FoldableItem> foldableItems;

        public FoldableAdapter(List<FoldableItem> foldableItems) {
            this.foldableItems = foldableItems;
        }

        public int getCount() {
            return foldableItems == null ? 0 : foldableItems.size();
        }
    }

    private int getLastRotation(float rotation) {
        return (int) rotation / 180 * 180;
    }

    private int getNextRotation(float rotation) {
        if (getLastRotation(rotation) == (int) rotation) {
            return getLastRotation(rotation);
        }
        return (int) rotation / 180 * 180 + 180;
    }

    private float getTouchX(TouchEvent touchEvent, int index) {
        float touchX = 0;
        if (touchEvent.getPointerCount() > index) {
            int[] xy = getLocationOnScreen();
            if (xy != null && xy.length == 2) {
                touchX = touchEvent.getPointerScreenPosition(index).getX() - xy[0];
            } else {
                touchX = touchEvent.getPointerPosition(index).getX();
            }
        }
        return touchX;
    }

    private float getTouchY(TouchEvent touchEvent, int index) {
        float touchY = 0;
        if (touchEvent.getPointerCount() > index) {
            int[] xy = getLocationOnScreen();
            if (xy != null && xy.length == 2) {
                touchY = touchEvent.getPointerScreenPosition(index).getY() - xy[1];
            } else {
                touchY = touchEvent.getPointerPosition(index).getY();
            }
        }
        return touchY;
    }

    public interface OnFoldRotationListener {
        void onFoldRotation(float rotation, boolean isFromUser);
    }

    public interface OnFoldingListener {
        void onUnfolding(UnfoldableView unfoldableView);

        void onUnfolded(UnfoldableView unfoldableView);

        void onFoldingBack(UnfoldableView unfoldableView);

        void onFoldedBack(UnfoldableView unfoldableView);

        void onFoldProgress(UnfoldableView unfoldableView, float progress);
    }

    public static class SimpleFoldingListener implements OnFoldingListener {
        @Override
        public void onUnfolding(UnfoldableView unfoldableView) {
        }

        @Override
        public void onUnfolded(UnfoldableView unfoldableView) {
        }

        @Override
        public void onFoldingBack(UnfoldableView unfoldableView) {
        }

        @Override
        public void onFoldedBack(UnfoldableView unfoldableView) {
        }

        @Override
        public void onFoldProgress(UnfoldableView unfoldableView, float progress) {
        }
    }
}
