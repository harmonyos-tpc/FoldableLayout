
FoldableLayout
==============

## Download
Gradle:

```groovy
repositories {
    mavenCentral()
}

dependencies{
    implementation 'io.openharmony.tpc.thirdlib:FoldableLayout:1.0.0'
}
```

## 核心 ##
本库的3D翻转的画面都是基于FoldableItem此类为基础。
FoldableItem 是一个可以支持3D翻转的对象，继承此类，并且重写onDraw(Canvas canvas, int width, int height) 设定需要3D翻转的画面


## 折叠型 ##

#### 效果图 ####
<img src="art/unfoldable.gif" width="40%"/>

#### 用法 ####

通过设置 public void unfold(FoldableItem coverFoldableItem, FoldableItem detailFoldableItem, Component coverComponent)
分别传入 封面的 FoldableItem， 详情 FoldableItem 以及 关闭时候封面时所处的Component 来实现折叠型的翻转动画。

## 翻页型 ##

#### 效果图 ####
<img src="art/foldable.gif" width="40%"/>

#### 用法 ####

通过设置 public void setAdapter(FoldableAdapter adapter)，而FoldableAdapter类中的主要参数是FoldableItem数组。
控件将获取该数组来刻画翻页型场景

#### License ####

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
